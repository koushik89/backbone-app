
/*global require*/
'use strict';

require.config({
	// The shim config allows us to configure dependencies for
	// scripts that do not call define() to register a module
	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: [
				'underscore',
				'jquery',
				'helpers'
			],
			exports: 'Backbone'
		},
		backboneValidation: {
			deps: ['backbone'],
			exports: 'Validation'
		},
		bootstrap: {
			deps: [
				'jquery'
			],
			exports: 'Bootstrap'
		},
		helpers: {
			deps: ['handlebar'],
			exports: 'Helpers'
		}

	},
	paths: {
		jquery: '../bower_components/jquery/dist/jquery',
		underscore: '../bower_components/underscore/underscore',
		bootstrap:'../bower_components/bootstrap/dist/js/bootstrap',
		handlebar:'../bower_components/handlebars/handlebars',
		backbone: '../bower_components/backbone/backbone',
		backboneValidation: '../node_modules/backbone-validation/src/backbone-validation',
		text: '../node_modules/requirejs-text/text',
		helpers:'handlebarHelpers'
	}
});

require([
	'backbone',
	'views/app',
	'routers/router'
], function (Backbone, AppView, Workspace) {
	/*jshint nonew:false*/
	// Initialize routing and start Backbone.history()
	//new Workspace();
	//Backbone.history.start();

	// Initialize the application view
	new AppView();
});