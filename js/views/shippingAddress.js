/*global define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'model/shipping',
	'text!templates/shippingDetails.hbs',
	'views/orderDetails',
	'bootstrap',
	'handlebar'
], function ($, _, Backbone, Shipping, shippingDetails, orderDetailsView, Bootstrap, Handlebars) {
	'use strict';

	// Our overall **AppView** is the top-level piece of UI.
var shippingAddressView = Backbone.View.extend ({
	model: new Shipping(),
	el:$("#shipping_details"),
	events: {
		'click .finish': 'showDetails',
		'change #address_one': 'changeAddressOne',
		'change #address_two': 'changeAddressTwo',
		'change #state': 'changeState',
		'change #city': 'changeCity',
		'change #zipcode': 'changeZipcode',
	},
	initialize: function(options){
		var product =options.product;
		var self = this;
		this.model.set('product',product);
		
		this.render();
	},
	changeAddressOne: function(e){
		this.model.set('addressone', $(e.currentTarget).val());
	},
	changeAddressTwo: function(e){
		this.model.set('addresstwo', $(e.currentTarget).val());
	},
	changeState: function(e){
		this.model.set('state', $(e.currentTarget).val());
	},
	changeCity: function(e){
		this.model.set('city', $(e.currentTarget).val());
	},
	changeZipcode: function(e){
		this.model.set('zipcode', $(e.currentTarget).val());
	},
	showDetails: function(e){
		e.preventDefault();
		this.$('.err-msg').text('');
		var me = this;
	    var valid = this.model.isValid();
	    this.model.set(this.model.attributes,{validate:true});

		this.model.on('invalid', function (model, error) {
	     if(valid){
	     	me.hideErrors(error);
	     }
	     else
	     	me.showErrors(error);
		});
		if(this.model.isValid()){
			me.$el.html('');
			var details = this.model.attributes;
			var shippingadd = new orderDetailsView({order:details});
		}

		

	},
	showErrors: function(errors) {
	    _.each(errors, function (error) {
	        var controlGroup = this.$('.' + error.name);
	        controlGroup.addClass('error');
	        controlGroup.find('.err-msg').text(error.message);
	    }, this);
	},

	hideErrors: function () {
	    this.$('.form-group').removeClass('error');
	    this.$('.err-msg').text('');
	},
	render: function(){
		var template = Handlebars.compile(shippingDetails);
		this.$el.html(template(this.model.attributes));
		return this
	}

	});
	return shippingAddressView;
});