/*global define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'model/orderModel',
	'text!templates/orderTemplate.hbs',
	'bootstrap',
	'handlebar'
], function ($, _, Backbone, orderModel, orderTemplate, Bootstrap, Handlebars) {
	'use strict';

	// Our overall **AppView** is the top-level piece of UI.
var orderDetailsView = Backbone.View.extend({
	model: new orderModel(),
	el:$("#order_details"),
	events: {
		
	},
	initialize: function(options){
		var product =options.order;
		var self = this;
		this.model.set('order',options.order);
		
		this.render();
	},
	render: function(){
		var template = Handlebars.compile(orderTemplate);
		this.$el.html(template(this.model.attributes));
		return this
	}
	});
	return orderDetailsView;
});