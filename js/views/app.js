/*global define*/
define([
	'jquery',
	'underscore',
	'backbone',
	'model/product',
	'text!templates/productDetails.hbs',
	'views/shippingAddress',
	'bootstrap',
	'handlebar'
], function ($, _, Backbone, Product, productDetails, shippingAddressView, Bootstrap, Handlebars) {
	'use strict';

	// Our overall **AppView** is the top-level piece of UI.
	var AppView = Backbone.View.extend({

		model: new Product(),
		el: $('#product_details'),
		events: {
			'click .next':	'showAddress',
			'change input[type=radio]': 'changedColor',
			'change select[name=size]': 'changeSize',
			'change input[type=number]': 'changeQuantity'
		},
		initialize: function(){
			var self = this;
			//this.template = Templates['productDetails.hbs'];
			this.render();
			this.model.on('change', function() {
				this.model.set('color',$("input[name=color]:checked").val());
				this.model.set('size',$("#size").val());
				this.model.set('quantity', $("#quantity").val());
				setTimeout(function() {
						self.render();
					}, 10);
				
					},this);
		},
		showAddress: function(){
			var self = this;
			var details = self.model.attributes;
			var shippingadd = new shippingAddressView({product:details});
			self.$el.html('');
		},
		changedColor: function(){
			this.model.set('color',$("input[name=color]:checked").val());

		},
		changeSize: function(){
			this.model.set('size',$("#size").val());
		},
		changeQuantity: function(){
			this.model.set('quantity', $("#quantity").val());
		},
		render: function(){
			var template = Handlebars.compile(productDetails);
			this.$el.html(template(this.model.attributes));
			return this;
		}
	});
	return AppView;
});