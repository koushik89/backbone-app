define([
	'underscore',
	'backbone'
], function (_, Backbone) {
	'use strict';

var Shipping = Backbone.Model.extend({
	defaults: {
		addressone:"",
		addresstwo:"",
		city:"",
		state:"",
		zipcode:"",
	},
    validate: function (attrs) {
	    var errors = [];

	    if (!attrs.addressone) {
	        errors.push({name: 'addressone', message: 'Please fill address field.'});
	    }
	    if (!attrs.city) {
	        errors.push({name: 'city', message: 'Please fill city field.'});
	    }
	    if (!attrs.state) {
	        errors.push({name: 'state', message: 'Please fill state field.'});
	    }
	    if (!attrs.zipcode || attrs.zipcode.length !== 5) {
	        errors.push({name: 'zipcode', message: 'Please enter valid zipcode.'});
	    }

	    return errors.length > 0 ? errors : false;
    },
});

	return Shipping;
});