define([
	'underscore',
	'backbone'
], function (_, Backbone) {
	'use strict';

	var Product = Backbone.Model.extend({
	defaults: {
		color:"blue",
		size:"small",
		quantity:"1"

	}
});

	return Product;
});
